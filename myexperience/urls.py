from django.urls import path

from . import views

app_name = 'myexperience'

urlpatterns = [
    path('', views.myexperience, name='myexperience')
]
